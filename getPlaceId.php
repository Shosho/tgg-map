<?php

/**
 * Ce fichier permet à l'utilisateur d'ajouter un fichier contenant la liste des toutes les structures dont il souhaite récupérer les informations.
 * On instancie un client HTTP et on interroge l'api Google Place pour récupérer les identifiatns de chacune des structures. On enregistre tout ça dans un fichier CSV qui l'on télécharge ensuite dans le navigateur de l'utilisateur.
 * Dans le cas où la structure est inconnue par Google et qu'aucun place_id n'est renvoyé; on l'enregistre tout de même dans le fichier sans autre information que son nom.
 */

declare(strict_types=1);

ini_set('memory_limit', -1);
set_time_limit(0);

require_once 'src/GoogleClient.php';

try {
    if ("" === $_FILES['filePlaceId']['tmp_name']) {
        echo 'Merci de fournir un fichier avant de valider.';
        die;
    }

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="placeIds.csv"');

    $fp = fopen('php://output', 'wb');
    fputcsv(
        $fp,
        [
            'PlaceId',
            'Nom',
            'Nom Google',
        ]
    );

    if (($handle = fopen($_FILES['filePlaceId']['tmp_name'], 'r')) !== FALSE) {
        $googleClient = new GoogleClient();    
        while (($data = fgetcsv($handle)) !== FALSE) {
            foreach ($googleClient->getPlaceId($data[0]) as $candidate) {
                fputcsv($fp, $candidate);
            }
        }
        fclose($handle);
      }

    fclose($fp);
}  catch (\Throwable $e) {
    if (isset($data[0])) {
        echo 'Une erreur est survenue pour '.$data[0].'.';
    }

    echo $e->getMessage();
    die;
}