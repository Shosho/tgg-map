
# Map TGG

## Avant toutes choses

1. Un glossaire est présent à la fin de ce document pour vous aider à le comprendre, certains termes techniques y sont expliqués. 
2. Ce projet utilise une API Google gratuite qui devient payante si son utilisation est trop fréquente. Sur [leur site](https://mapsplatform.google.com/pricing/?hl=fr#places), Google précise que la facturation ne se déclenche qu'après `28500` appels par mois. Pour information, ce projet appelle l'API une fois par ligne présente dans le fichier Excel. Cette limitation maximum est en fait limitée à moins de `1000` appels par jour, alors attention à ne pas la dépasser.
3. Ce projet est améliorable et peut-être améliorer sur demande et après étude.
4. Ce projet n'a pas pour prétention de répondre à toutes les problématiques liées à la map, il n'a pour ambition que de faciliter ce travail.

## Introduction

Ce projet a pour but d'interroger l'API Google nommée Place pour y récupérer les informations des différentes structures présentes sur le site [map.thegreenergood.fr](https://map.thegreenergood.fr/) et de créer un fichier CSV contenant les informations renvoyées.

Deux API sont utilisées sur ce projet :
- PlaceSearch qui permet de récupérer l'ID Google, appelé `place_id`, d'une structure
- PlaceDetails à qui l'on demande les données des structures (horaires, adresse, etc.) en lui fournissant un `place_id` que l'on aura récupéré plus tôt

## Installation

Ce projet a été programmé en PHP, cela signifie qu'il faut installer ce logiciel afin de pourvoir le lancer.

- Télécharger ce projet sur l'ordinateur, l'enregistrer et l'extraire dans le répertoire `C:\` de votre ordinateur grâce à un logiciel comme [Winrar](https://www.win-rar.com/download.html). Cela va créer un dossier nommé `C:\tgg-map-main`, si le nom diffère, renommer le dossier.
- Aller ensuite à l'adresse [https://www.php.net/downloads.php](https://www.php.net/downloads.php) puis cliquer sur "Windows downloads".
- Dans la fenêtre qui s'ouvre, télécharger le premier fichier en cliquant sur `Zip` et le décompresser à l'aide d'un logiciel comme [Winrar](https://www.win-rar.com/download.html) dans le répertoire `C:`. Cela devrait créer un dossier nommé `C:\php`, si le nom diffère, renommer le dossier.
- Ouvrir un terminal Windows (aussi appelé `Invite de commandes`) en recherchant `Terminal` dans le menu démarrer ou en appuyant sur les touches `Windows+R` puis en tapant `cmd` dans la fenêtre qui s'ouvre puis en appuyant sur la touche "Entrée".
- Dans la fenêtre qui s'ouvre, taper (ou copier/coller) la commande `move C:\tgg-map-main\php.ini C:\php\php.ini`. Note : Il est possible que le message suivant s'affiche : `Remplacer C:\php\php.ini (Oui/Non/Tous) :`. Si tel est le cas, tapez le mot `Oui` puis appuyez sur la touche entrée.
- Une fois l'instruction précédente terminée, taper (ou copier/coller) la commande `cd C:\tgg-map-main && C:\php\php.exe -S localhost:8000` dans cette même fenêtre.
- Ouvrir un navigateur internet et taper `localhost:8000` dans la barre d'adresse.
- Après avoir fini, il suffit de fermer la fenêtre du terminal pour que tout s'arrête. La prochaine fois que vous aurez besoin du projet vous n'aurez qu'à effectuer que les deux étapes précédentes.

### Mais qu'est-ce qu'on vient de faire ?

Déjà je suis désolé de vous avoir fait faire tout ça, je sais que ce n'est pas évident mais sans ça il est strictement impossible de lancer le projet.  
Mais pas de panique, tout ce que je vous ai fait faire est sécurisé et ne présente aucun risque. Dans l'ordre on a :
- Téléchargé le projet depuis un serveur
- Téléchargé, installé et configuré PHP qui est simplement un outil pour exécuter le code du logiciel
- Ouvert un terminal, copié un fichier de configuration nécessaire au bon fonctionnement de l'application et lancé le serveur PHP qui permet de faire tourner le logiciel
- Enfin, on a simplement ouvert le projet dans un navigateur afin de l'exécuter, c'est tout :)

## Utilisation

Une fois le projet lancé dans un navigateur vous pouvez :
- Importer un fichier au format `.csv` listant toutes les structures présentent sur le site (ce fichier est d'ailleurs présent sur le projet, [ici](https://gitlab.com/Shosho/tgg-map/-/blob/main/file.csv)). En cliquant sur le bouton le serveur va interroger Google pour récupérer les identifiants de chacune des structure puis il va vous télécharger un fichier au format `.csv` contenant l'identifiant Google, le nom de la structure et le nom de la structure renvoyée par Google.
- Le second bloc permet d'importer un fichier `.csv` contenant une liste de l'identifiant Google des structures et de leur nom (le fichier téléchargé précédemment si vous suivez) permettant d'interroger à nouveau l'API Google mais pour cette fois récupérer toutes les informations que l'on peu trouver sur la structure comme ses horaires, son adresse, son téléphone, etc.

## Informations techniques

Comme dit plus haut, le projet est en PHP, en version 8.2. Il y a un peu d'HTML5 et quelques lignes de CSS mais rien de bien fou, c'est simplement pour que ce soit plus agréable à l'utilisation.

En cliquant sur un bouton de la page d'accueil le fichier ajouté est envoyé au serveur qui va le parcourir et interroger Google pour récupérer les informations. Tout ça se trouve dans le fichier `src/GoogleClient.php`, le code devrait se suffire à lui-même pour la compréhension. Les informations que l'on demande à Google sont renseignées dans la constante `FIELDS` présente dans ce fichier.

Le code n'est pas le plus beau, j'aurais pu faire plus propre, ajouter des tests unitaires et même faire un peu de qualité avec un linter ou autre mais ce n'est pas le cas, je voulais garder de la simplicité et éviter à quiconque d'installer moult librairies pour faire tourner le projet. 

## Glossaire

- API : Terme technique qui désigne une interface logicielle que l'on peut interroger pour récupérer des informations. En bref, c'est une adresse que l'on appelle pour demander des données.
- CSV : C'est une extension de fichier. Ces fichiers sont des tableurs dans lesquels les données sont séparées par des virgules, `csv` signifiant "Comma-separated values". Dans un fichier Excel (`.xls`, `.xlsx`, `.xlsm`) les données sont séparées par des points-virgules ou des tabulations. Mais aucune inquiétude : les logiciels Excel ou GoogleSheet savent lire un fichier en `.csv`, ils vont simplement changer les séparateurs lors de l'import du fichier.
- ID : C'est un identifiant technique unique qui, chez Google et pour cette API, ressemble à `ChIJba44H4Xq9EcRZNg2rDzrgns`
- PHP : C'est un langage informatique dédié au développement web. WordPress est développer en PHP par exemple. Pour pouvoir exécuter du code PHP il est nécessaire de passer par un serveur web. C'est pour cette raison que je vous en ai fait lancer un via la commande `cd C:\tgg-map-main && C:\php\php.exe -S localhost:8000`. Pour ceux que ça intéresse, cette commande est en deux parties séparées par l'opérateur logique `&&` qui veut simplement dire qu'on exécute la première instruction de la ligne puis la seconde. Dans l'odre donc, la commande `cd C:\tgg-map-main` permet de se déplacer dans le dossier du projet et la commande `C:\php\php.exe -S localhost:8000` permet de dire à PHP de lancer un serveur dans ce dossier.
- HTML : C'est le langage qui permet d'afficher une page web, `<p>Bonjour TGG</p>` permet d'afficher un paragraphe contenant le texte `Bonjour TGG`.
- CSS : C'est le langage qui permet de styliser une page web, `p {color: white}` permet de faire apparaître le texte des paragraphes de la page en blanc.

## Limitations

- Pour créer le premier listing des structures j'ai récupéré toutes les structures sur la map. Cependant je n'ai pu récupérer que leur nom et aucune autre information. Ainsi, pour éviter de se retrouver avec des structures venant de toute la France, j'ai limité à Lyon et ses environs. Ainsi une recherche pour, par exemple, 3 p'tits pois sera envoyée à Google comme "3 p'tits pois Lyon". Ainsi, les lieux qui sont plus éloignés ou qui sont des sites web qui n'ont pas d'adresse physique vont être mal ou pas récupérés par Google.
- La première recherche effectuée sur l'application renvoie une liste non exhaustive de tout ce que Google trouve pour les critères entrés. Cela va nous permettre de lister toutes les structures avec leur identifiant Google (placeId) mais il faudra effectuer un premier travail de tri pour être sûr de ne récupérer que les structures présentes dans la map. Ainsi, une fois ce tri fait, nous ne nous baserons plus que sur cet identifiant pour faire nos recherches et non plus sur un nom qui est sujet à erreur.

## Récupérer un PlaceId manuellement

Il existe plusieurs solutions pour récupérer un nouveau place_id pour une structure.
1. La première consiste à créer un fichier .csv vide et d'y ajouter simplement le nom de la structure. Il faut ensuite uploaded ce fichier dans l'application via la case "Place ID" et récupérer le fichier téléchargé.
2. La seconde nécessite quelques connaissances en développement mais elle a un avantage : elle ne compte pas dans le quota du nombre de recherche de l'API. Il faut dans un premier temps rechercher la structure sur Google, une fois cela fait il faut ouvrir la console de développement (en faisant un click droit puis `inspecter` ou en appuyant sur la touche `F12`). Cliquer ensuite sur `Console` puis copier/collier la ligne de code suivante : `document.querySelectorAll('[data-pid]')[0].attributes['data-pid'].value`. Cela affichera un petit texte correspondant au `place_id` de l'établissement.
3. La dernière nécessite également un peu de connaissance. Le lien suivant permet de récupérer le nom, l'identifiant et l'adresse d'une structure. Il suffit de remplacer le texte `%NOM%` par le nom de l'établissement que vous souhaitez rechercher. `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=%NOM%&fields=name,place_id,formatted_address&inputtype=textquery&key=AIzaSyAXZEERtvgxZAcCnP4TFM-sXCM9GD3Rw7U`.