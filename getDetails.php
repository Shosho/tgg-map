<?php
/**
 * Ce fichier permet d'instancier un client HTPP qui va ensuite appelé l'API Place Details pour récupérer les informations relatives à chaque structure présente dans le fichier ajouté par l'utilisateur.
 * On créé ensuite un tableur en .csv avec toutes les informations renvoyées par l'API. Si l'une des structures n'a pas de place_id, on l'ajoute tout de même au tableur mais toutes les informations seront vides. Cela peut arriver si la structure est un site web sans adresse physique.
 * Une fois l'écriture terminée, on télécharge le fichier dans le navigateur de l'utilisateur.
 */

declare(strict_types=1);

ini_set('memory_limit', -1);
set_time_limit(0);

require_once 'src/GoogleClient.php';
require_once 'src/Place.php';

try {
    if ("" === $_FILES['fileDetails']['tmp_name']) {
        echo 'Merci de fournir un fichier avant de valider.';
        die;
    }

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="tgg.csv"');

    $fp = fopen('php://output', 'wb');
    fputcsv(
        $fp,
        [
            'PlaceId',
            'Status',
            'Adresse',
            'Nom Google',
            'Nom TGG',
            'Téléphone',
            'Lundi',
            'Mardi',
            'Mercredi',
            'Jeudi',
            'Vendredi',
            'Samedi',
            'Dimanche',
            'Site',
        ]
    );

    if (($handle = fopen($_FILES['fileDetails']['tmp_name'], 'r')) !== FALSE) {
        fgetcsv($handle);
        $googleClient = new GoogleClient();
        while (($data = fgetcsv($handle)) !== FALSE) {
            fputcsv(
                $fp,
                (new Place(
                    "" !== $data[0] ? 
                        $googleClient->getDetails($data[0]) :
                        [
                            'name' => $data[1],
                        ]
                )
            )->toCsv($data[1]));
        }
        fclose($handle);
    }

    fclose($fp);
}  catch (\Throwable $e) {
    if (isset($data)) {
        echo 'Une erreur est survenue pour l\'id '.$data[0].'.';
    }

    echo $e->getMessage();
    die;
}