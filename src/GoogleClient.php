<?php

/**
 * Ce fichier contient les informations relatives aux API Google.
 * Ici se trouvent les adresses que nous appelons dans le projet, l'API Key pour accéder aux ressources et les champs que l'on souhaite récupérer dans l'API.
 * Toutes les informations relatives à l'API Place se trouvent ici https://developers.google.com/maps/documentation/places/web-service/details?hl=fr.
 */

declare(strict_types=1);

final class GoogleClient
{
    private const KEY = '';
    private const PLACE_SEARCH_URL = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=%INPUT%&inputtype=textquery';
    private const PLACE_DETAILS_URL = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=%PLACE_ID%&language=fr';
    private const FIELDS = [
        self::PLACE_SEARCH_URL => [
            'name',
            'place_id',
        ],
        self::PLACE_DETAILS_URL => [
            'place_id',
            'business_status',
            'formatted_address',
            'name',
            'formatted_phone_number',
            'opening_hours/weekday_text',
            'website',
        ],
    ];

    public function getPlaceId(string $shopName): array
    {
        $result = file_get_contents(
            str_replace(
                '%INPUT%',
                rawurlencode($shopName . ' Lyon'),
                $this->buildUrl(self::PLACE_SEARCH_URL)
            )
        );

        if (false === $result) {
            throw new \Exception('Une erreur est survenue lors de l\'appel à l\'API "Place Search".');
        }

        $arrayResult = json_decode($result, true);
        if (0 === count($arrayResult['candidates'])) {
            return [['', $shopName, '']];
        }

        $places = [];
        foreach ($arrayResult['candidates'] as $item) {
            $places[] = [$item['place_id'], $shopName, $item['name']];
        }

        return $places;
    }

    public function getDetails(string $placeId): array
    {
        $result = file_get_contents(
            str_replace(
                '%PLACE_ID%',
                $placeId,
                $this->buildUrl(self::PLACE_DETAILS_URL)
            )
        );

        if (false === $result) {
            throw new \Exception('Une erreur est survenue lors de l\'appel à l\'API "Place Details".');
        }

        return json_decode($result, true)['result'];
    }

    private function buildUrl(string $url): string
    {
        return $url . '&fields=' . implode(',', self::FIELDS[$url]) . '&key=' . self::KEY;
    }
}