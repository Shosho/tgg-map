<?php

/**
 * Cette classe décrit ce qu'est une structure d'un point de vu code.
 * Elle permet de créer une structure, une Place, et de renvoyer ses données sous la forme d'un tableau.
 */

declare(strict_types=1);

final class Place
{
    private string $place_id = '';
    private string $business_status = '';
    private string $formatted_address = '';
    private string $name = '';
    private string $formatted_phone_number = '';
    private array $opening_hours = [];
    private string $website = '';
    private bool $delivery = false;
    private bool $dine_in = false;
    private bool $serves_breakfast = false;
    private bool $serves_brunch = false;
    private bool $serves_dinner = false;
    private bool $serves_lunch = false;
    private bool $serves_vegetarian_food = false;
    private bool $takeout = false;
    private array $geometry = [];

    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function toCsv(string $nameTgg): array
    {
        $days = [];
        foreach ($this->opening_hours['weekday_text'] ?? [] as $key => $opening) {
            $matches = [];
            preg_match('/(\w+): (.*)/', $opening, $matches);
            $days[$matches[1] ?? ''] = $matches[2] ?? '';
        }

        return [
            'PlaceId' => $this->place_id,
            'Status' => $this->getStatus($this->business_status),
            'Adresse' => $this->formatted_address,
            'Nom Google' => $this->name,
            'Nom TGG' => $nameTgg,
            'Téléphone' => \str_replace(' ', '', $this->formatted_phone_number),
            'Lundi' => $days['lundi'] ?? '/',
            'Mardi' => $days['mardi'] ?? '/',
            'Mercredi' => $days['mercredi'] ?? '/',
            'Jeudi' => $days['jeudi'] ?? '/',
            'Vendredi' => $days['vendredi'] ?? '/',
            'Samedi' => $days['samedi'] ?? '/',
            'Dimanche' => $days['dimanche'] ?? '/',
            'Site' => $this->website,
        ];
    }

    private function getStatus(string $businessStatus): string
    {
        switch ($businessStatus) {
            case 'CLOSED_PERMANENTLY':
                return 'Fermé définitivement';
            case 'CLOSED_TEMPORARILY':
                return 'Fermé temporairement';
            default:
                return '';
        }
    }
}